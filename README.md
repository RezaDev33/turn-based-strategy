# Turn Based Strategy

Ini adalah Game dengan tipe 4x turn based strategy games yang dibuat dengan [Godot](https://godotengine.org/) game engine. Game ini dibuat sebagai hobby dan untuk mengasah kemampuan programming dan desain. Untuk detail lebih lengkap ada pada file [Game_Desain_Document.docx](/Game_Desain_Document.docx).
